# Report: Follow-up Interviews - Costa Rica - Internews

As a follow up of the previous activities ([unmoderated test](https://gitlab.torproject.org/tpo/ux/research/-/blob/master/reports/2021/UR-Tor-CostaRica.md) and [survey](https://gitlab.torproject.org/tpo/ux/research/-/blob/master/reports/2021/Report_Follow-up_Survey_-_Costa_Rica_-_Internews.md)), the goal of this research was to understand if they would recommend (or not) Tor Browser. To do so, I conducted 5 semi-structured interviews with the same participants I've been studying. In this phase, the idea was to explore more deeply the reasons behind their recommendation (or not) of TB. The interview consisted of 9 guiding questions, but all the interviews were different and we ended up focusing on whatever the participant was mostly interested in discussing.

Author: [@josernitos](https://gitlab.torproject.org/josernitos) - Funded by: Internews - Feedback Collection Funding Pool 2021

## Main takeaways from study

Although participants would recommend Tor Browser, they would only recommend it in extreme cases where it's a necessity. Specifically, they would recommend it to anyone who's involved in a sensitive issue, an activist, a human rights defender, or someone which their life depends on their anonymity. In short, their understanding was that (i) Tor Browser was something to be used only when their life is in danger, and not as a security best-practice (ii) they believed the browser can't be used in their day to day life. This is troublesome since if all users draw a similar conclusion then all traffic would be directed towards specific goals which might make it easier to spot those users.

The reasons they gave for these conclusions (i & ii) were varied but they mainly stem from technical and cultural barriers, the way they think about online workflow, and the kind of person they thought would put up with these problems. 

##### Technical barriers

The more evident barriers to the participants were the technical ones. Some of the technical barriers they mentioned were:

- Speeds were slow.
- They couldn't access some websites.
- Had errors while using some websites.
- Some of the errors had technical language and they didn't know what to do.
- They had to constantly accept/deny cookies from the sites they visited.
- They were prompted to verify their browsing as they were continuously targeted by captchas.
- They had to manually log every time to websites as the browser would not remember their credentials.

##### Cultural barriers

In trying to extrapolate these barriers onto the population at large they discussed their view of how society uses and understands technology. 

- As humans, we like what's familiar to us.
- Not all of us are technical proficient or work in a technological area.
- We don't really know how the technologies work, we just mainly use them.
- We are not critical of the technologies we use.
- We don't understand the value of these technologies.
- People might view some technologies as unnecessary for our daily life. 
- We are ignorant of how corporate technologies limit our rights.

##### Online workflow

The understanding of how we view and adopt technologies leads us to how we use and expect new technologies to fit in our mindset. But more so, in our digital ecosystem. Here, the participants were clear as to how the interaction with Tor Browser disrupted their way of working and using the Internet. 

In essence, they are used to a browser that stores their information, from passwords, URL addresses, credit card information, billing addresses, etc. And when your workflow is defined by using (logging to) and visiting many different websites, then the expectation of storing or recording these information leads to a feeling of unnecessary friction. 

In their point of view, the browsing experience should be supportive of their everyday needs and Tor Browser was not meeting them. The constant prompt to accept/deny cookies to websites they have previously visited, the need to manually log in to every site and continuously write captchas, the incapacity to remember a URL they visited before, the errors that some sites showed (which at best scenario it didn't worked as expected and at worst scenario it didn't worked at all), and finally the errors showed by the browser but which they didn't understand. All these problems lead to discouraging the use of the browser, and to only use it in specific cases.

##### Person who might put up with these frictions

This context made them to believe that a person willing to use Tor Browser is someone with patience, eager to learn and understand the technology. They indicated that, as any other software, you can look online for more information about the technology and get a better understanding of it. But the amount of users that would do such a search for clarity is limited. And although a participant talked about the FAQ on the TB website as something useful, they thought the explanation was still very technical.

Or, as I pointed out at the beginning of the report, a person who might put up with these frictions would be a person that needs this technology. To them, is not a question of choice but rather a life or death issue. An issue that even though they have to struggle with all these barriers, they will continue to use the software since there are no other alternative browsers that will help them in their situation.

##### What could Tor Browser do?

As part of the interview process, I also asked whether they had ideas on how to solve these problems, and their ideas gravitated towards explaining the tool, as well as having a reliable community in case they needed help. 

- Create a more familiar design. From the interface to some of the processes or screens.
  - Create an onboarding or guide that explains TB. 
    - Explaining throughout tips that are prompted during use.
      - Like the Microsoft Office Clip that follows you around.
    - Or short videos explaining how to use it and why is important. 
      - Sort of like short tiktoks videos or animations.
    - A much more human and less technical FAQ.
  - Design it for a more habitual use and not a one-time use.
  - Fix or explain why it has unexpected errors.
- Gamify the use of Tor Browser
  - Show the number of threats disabled, or how many connections they've been using throughout a session. 
- Create a community of support.
  - This way, if a user has questions they can ask for help and they can explain in simple language.
- Create courses on how to use Tor Browser.
  - If possible, have courses in person and around a community.
  - Or as a digital course that you can take.
- Explaining digital rights and our need of using tools like Tor Browser
  - Workshops, courses, etc. about the importance of our rights to privacy and anonymity. 

## Discussion

After creating this three phase research, is interesting to see again and again one of the [first concerns I raised](https://gitlab.torproject.org/tpo/ux/research/-/blob/master/reports/2021/UR-Tor-CostaRica.md#main-takeaways-from-study). At the beginning of the study I felt that participants did not understood the value of Tor Browser. Mainly, that Tor Browser let's you access the Internet in a completely different way. But as we have continued to deepen our understanding with the same participants throughout the study, I believe they are right to expect a browser to behave like a browser.

Perhaps we are not communicating enough the inner workings of the browser. Perhaps we should clearly show the reason why they are experiencing those kind of technical barriers, given the technology. What is clear and what I think we can learn from this research is that the expectations and needs of any user accessing the Internet through a browser are crucial. This doesn't mean to get rid of what makes Tor Browser be Tor Browser, but rather that there is room for improvement. In essence: how can we create a similar experience to other browsers but at the same time continue to fight for human rights and freedom? Not either/or but rather doing both things together. Fighting for people that would otherwise be incommunicated and also maintaining the best standard of web browsing experience.
