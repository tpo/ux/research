# Checklist for participants - Facilitators and/or Observers

Hi, we are starting our round of Bridges interviews: Here is some info for coordination:

**Scheduled interviews:** You can find them at the "Participants" sheet [link to Participants spreadsheet]

**Participation**: If you want to participate, you're welcome to join us. To avoid being overwhelming with interviewers (and to avoid bandwidth issues), its probably the best to have between 2 and 3 interviewers per session. Please add your name in the spreadsheet. Please add your name in the column G before hand. [link to Participants spreadsheet]

**Interview format:** I'll facilitate the structured interview by asking questions and sharing tasks to users.
You're welcome to participate in hearing the feedback first-hand, observe how they use Tor Browser, and take notes as well.

**Interview structure:**

1. Welcome and consent form
2. Demographics
3. Questions and tasks - Moderated usability test
4. Open floor
5. Thank you!

**Interview questions and tasks:** script and instructions are detailed here: [link to resources in https://gitlab.torproject.org/tpo/ux/research/-/tree/master/scripts%20and%20activities]

**Note-taking:** We will be taking notes in the spreadsheet, under each of the questions. I have added a row for each (scheduled) interviewee, where we can take note of their feedback/answers to each question. 

[link to Participants spreadsheet]

**User-reflections session:** At the end of our week of interviews, we will meet to discuss findings and interesting highlights for the reporting before making it public. You are welcome to join us as well!

If you have any questions or suggestions on how we can potentially do the interviews differently or improve the process, feel free to reach me directly.

Thank you for participating in this user research! 

The Tor UX team
