## Features Survey

### User Research Plan - Features Survey

We want to investigate and survey selected participants using card sorting to find which are their preferable features for the still called Tor-vpn. This survey will have open and close-ended questions, so it doesn't aim to a broad audience. We will ask selected participants from past studies to participate and give their feedback.

#### Goals

* Find most preferable features by users
* Find which features can be missed
* Understand users’ preferences on their current product

#### Audience

human rights defenders, journalists, activists.

#### Recruitment

We will ask participants from past surveys and other research studies to engage in this study.

#### Previous Research
selected tickets.

#### Methodology

Online survey, card sorting.

The card sorting survey is an interactive question that can provide a better understanding about users needs and engagement on features and services. Sorting the features items into categories will provide the ability to get feedback on numerical value, quickly bringing insights to the user research and next approaches about the product.

### Suggested Time

10-15 minutes

#### Survey script

##### Description

This study is exploring a Minimum Viable Product (MVP) for a Tor-powered VPN software. For that matter, the main goal is to understand which features users want in this software that is not already included. We will ask you to choose your preference among some features and classify them in order of preference. We also want to understand a bit more about your uses of VPN, and we appreciate it if you can explore this as much as possible. Your support in this study is very important to build this product to the users needs.

´First´ 

##### Disclaimer:

A Tor-powered VPN will be a free and open source software, available to anyone that wants to use an anonymous and untraceable Tor-powered VPN. In this software, it is aimed to provide a high privacy and security levels, but acknowledge that it depends on the application that the user wants to use with Tor.

* Where are you based at the moment? list of countries
* What VPN do you use? list of options + other (please specify if you selected “other”)
* Why did you choose this VPN? free text
* What do you use the VPN for? free text
* In what devices do you use this VPN?

´Second´

##### Connection:

Please sort the attributes below into the groups on the right in regards to:

* must-have 	
* nice-to-have 	
* doesn't need

- I want to see the status of both my internet and Tor connection 	
- I want to see the circuit each app is being routed through 	
- I want to receive a warning if I loose connection to Tor 	
- I want to see a chart of my up/down data over time

##### Privacy and Security

Please sort the attributes below into the groups on the right in regards to:

* must-have 	
* nice-to-have 	
* doesn't need

- I want to immediately block all traffic from my device if my Tor connection drops out, so I can avoid compromising my privacy 	
- I want to connect to Tor automatically when I restart my device
- I want to prevent any traffic leaking in the clear net when I'm using an app that is set to be routed through Tor so I can preserve my privacy and anonymity
- I want to customize my security settings so I can use it as predetermined options for security and privacy for each app
- I want to be made aware about the risks of using apps that can risk my security and privacy, so I can make an informed decision about whether to proceed
- I want to ensure that each app uses different circuit, so I can prevent my activity from one app being linked to another 

##### Censorship Circumvention

Please sort the attributes below into the groups on the right in regards to:

* must-have 	
* nice-to-have 	
* doesn't need

- When the Internet is blocked in my location I want to find a circumvention tool that doesn’t charge or feature data caps
- I want to detect and circumvent censorship automatically, so I can connect to Tor and overcome further censorship without needing to configure my connection manually
- I want to be able to manually configure my connection, so I can connect to Tor using custom connection settings, and without providing my location
- I want to be able to select from a list of bridges, so I can connect to Tor and circumvent censorship
- I want to be able to find alternative bridge lines that are more likely to succeed, so I can provide a bridge manually
- I want to be able to enter the text string in the Tor app manually, so I can connect to Tor and circumvent censorship using the bridge line I’ve been provided with 
- I want to be able to scan the QR code within the Tor app, so I can connect to Tor and circumvent censorship using the bridge line I’ve been provided with
- I want to be able to share that bridge with my peers, so I can help them circumvent censorship and connect to Tor
- I want to be able to choose which apps I do and don’t route over Tor, so I can unblock certain apps without slowing my connection or running into issues with others

##### Onion services

Please sort the attributes below into the groups on the right in regards to:

* must-have 	
* nice-to-have 	
* doesn't need

- I want to enter the onion site’s address in my web browser of choice, so I can access the onion service privately using my regular web browser 	must have 	nice to have 	doesn't need
- I want to be notified of its existence and linked to the onion address , so I can discover new onion sites and switch over to them where available 	must have 	nice to have 	doesn't need
- When I visit a broken onion site address in my browser I want to receive a warning detailing the issue, so I can understand the issue and/or the risks of proceeding, if applicable 	must have 	nice to have 	doesn't need
- I want to see a list of bookmarks to popular onion sites, so I can discover and visit new onion sites 

##### Tor Community 

Please sort the attributes below into the groups on the right in regards to:

* must-have 	
* nice-to-have 	
* doesn't need

- I want to be able to donate to The Tor Project from within the Tor app itself 	must have 	nice to have 	doesn't need
- I want to be able to subscribe to a news feed so I can stay up to date with the latest Tor news and updates 	must have 	nice to have 	doesn't need
- I want to be able to access links to help documentation and support channels through the app 	must have 	nice to have 	doesn't need
- I want to be able to view the help documentation offline 	must have 	nice to have 	doesn't need
- I want to file a report directly from the app itself 	must have 	nice to have 	doesn't need
- I want to be notified within the app to participate in the survey and provide my feedback to the app’s developers 	must have 	nice to have 	doesn't need
- I want to be able to view the Tor logs so I can debug the issue, and forward the logs to others who can help if appropriate 	must have 	nice to have 	doesn't need



### Final

We’re recruiting people to participate in a diary study for the Tor-powered VPN. In this study, the participant will make daily notes about their use of their preferred VPN, and at the end of the week they will participate in a moderated remote video-recorded discussion with other participants. We’re offering $$$ for selected participants. If you’re willing to participate, please type your e-mail address so we can contact you later. 

### Report Findings

### Attachments


