# User Research Script - Get to know the User

## 1 Starting

Rationale: Understanding if the user is a power user or average user. How they use Tor, if they use Tor with VPN, what browser and VPNs do they use and what for.

## 2 Intro

Hello! Thank you for taking the time to allow us to interview you. We are testing the $PRODUCT, and this time we are going to research $NAME-RESEARCH-PLAN. It is different from what you have been using/seeing before, so it will be helpful if you could think aloud and say toughts as they occur.

1. What types of devices do you use to access the Internet?

- Mobile/cell phone
- Tablet
- Desktop or laptop computer
- Other:   


2. How often do you use Tor?

- At least once a day
- At least once a week
- Just in specific situations
- I'm a first time user


3. Which of the following Tor-powered products do you use?

- Tor Browser for Desktop
- Tor Browser for Android
- Onion Browser
- OnionShare
- Orbot
- Tails
- SecureDrop
- Other:   

4. How often do you use a VPN?

- At least once a day
- At least once a week
- Just in specific situations
- I'm a first time user

5. Which VPN do you use?

6. What gender do you identify as?

- Female
- Male
- Non binary
- Prefer not to disclose

7. In which country are you based at the moment?


8. On a scale of 1 to 5, how strongly do you believe you need to protect your privacy and security online?

Consider an answer of 1 as "I don't think I need protection", and 5 as "I really need protection"

9. What is your email address?

10. Do you agree with us contacting you again in the future for other similar activities?
