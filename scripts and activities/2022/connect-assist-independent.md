# User Research Plan

**What we are testing**: Connect Assist, censorship circumvention, speed.

**Who is testing**: users in a restricted and/or censored area.

**Goals:**

- identify how long it takes for Tor Browser to identify censorship
- identify how long it takes for Tor Browser to give the user an useful bridge
- understand speed variation depending on website location

Findings will be use to create recommendation to developers in order to make the experience of connecting to Tor Browser better.

**What you will need:**

- Internet (to download Tor Browser and browse websites)
- Computer laptop or desktop

This test can take from 5 to up 15 minutes.

## Instructions

**Important: read all instructions before starting.**

1. If you use a VPN, turn off your VPN before proceeding;

2. Download Tor Browser Alpha 

- from https;//torproject.org/download/alpha/, 
- if torproject.org is  blocked, you can download it from one of our mirrors:  http://tor.calyxinstitute.org/download/alpha/

3. Install Tor Browser Alpha

4. Prepare to take notes (use a notepad or a notebook with a pen/pencil)

5. Run Tor Browser Alpha

6. There will be an option to `Connect` or `Configure connection`, click on `Connect`

7. Take notes of how long Tor Browser takes to identify censorship (i.e. 2 minutes, 15 seconds, etc)

**Notes:**

8. Once censorship is identified, it will ask you to share your location with the browser (your location is not shared to anyone but the software) to find an useful bridge for your case.


9. Take notes of how long it takes for Tor Browser to connect  (i.e. 2 minutes, 15 seconds, etc)

**Notes:**

10. After connected, visit a regular website (like google.com)

10.1. take  notes about how long it takes to connect you to the website

**Notes:**

10.2. check your  circuit (when you're visiting the website, there will be a padlock close to the website address, click there and you will see the circuit)

10.3. verify what bridge is using and take notes about it, for example: `Bridge: snowflake`.

**Notes:**

11. Visit another regular website 

11.1. take notes about how long it takes to connect to the website.

**Notes:**

12. Visit three websites from your location

12.1. take notes of how long it takes to connect to them.

**Notes:**

1.

2.

3.


Use this instruction to fill with your results and send it to nah@torproject.org.

Thank you very much for you help!


UX Team

The Tor Project
