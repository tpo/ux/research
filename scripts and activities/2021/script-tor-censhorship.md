**Intro**

Hi XX, Thank you for talking to us today! We've been following the recent events in $country with concern, and wish to support you when it comes to accessing Internet.

In this conversation, we want to go through two steps. First, we want to ask you some demographics questions - so we can better understand how you use Tor Services. Then we will move to some questions about what's going on in $country.

Please, feel free to say and ask anything about this process and Tor. We are here to answer your questions too! Also, if you want to stop this conversation at any moment, just let us know!

**Q1** We saw in the news that because of recent political events, there was an Internet Shutdown in your country, how was that for you? What do you remember about this day? (_ice-breaking question, to understand user needs_)

**Q2** Were you able to access the Internet those days?

-   [YES] How so?
-   [NO] Did you talk to anyone about it? What were the measures you take to access the internet or to understand the problems?

**Q3** Besides those moments, what are the challenges you face when accessing the Internet? And social media?

**Q4** Do you use a VPN?

-   [YES] Which one? [...] Why did you choose this one? Do you have any problems when using a VPN? Like, connectivity issues?
-   [NO] How do you access websites that are censored in your country? Or social media?

**Q5** Can we move to accessing the Internet through Tor browser? [...] If you're using a VPN, please turn it off, then access torproject.org [_if user succeeds accessing TB without a VPN, then download, install, and browser a news website; if not, then move to a mirror and repeat_]
