## Consent Form

I agree to participate in the study conducted by The Tor Project. 

I understand that participation in this study is voluntary and I agree to immediately raise any concerns or areas of discomfort during the session with the UX Researcher.

This Research Participation Agreement (“Agreement”) is between the UX Researcher __________________________ and the undersigned person or entity ______________________.

We are carrying out _______________________ to gain insights into how users engage with our services and us as an organization. This research will be conducted by the UX Research volunteer ____________________.

In consideration of your agreeing to participate in the Research, the UX Researcher and you agree as follows (please state "YES" or "NO"):

- [    ] To audio record this study.
- [    ] To video record this study.
- [    ] To take notes during this study.
- [    ] To contact me by e-mail if need any clarifications, _____________________________.
- [    ] To share my location (city/state/country if needed in this study).

The UX Researcher wont't upload any recording or notes to third-party services/clouds. Any recordings and notes will be destroyed by the UX Researcher after analyzing the results of this study.

Please sign below to indicate that you have read and you understand the information on this form and that any questions you might have about the session have been answered. 


#### Date:


### Participant


Please print your name: 


Please sign your name: 


#### UX Researcher


Plase print your name:


Please sign your name:


### Thank you!

We appreciate your participation.
