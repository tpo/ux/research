# User Research Script - Get to know the User

## 1 Starting

*Rationale*: Understanding if the user is a power user or average user. How they use Tor, if they use Tor with VPN, what browser and VPNs do they use and what for.

## 2 Intro

Hello! Thank you for taking the time to allow us to interview you. We are testing the $PRODUCT, and this time we are going to research $NAME-RESEARCH-PLAN. It is different from what you have been using/seeing before, so it will be helpful if you could think aloud and say toughts as they occur.

## 3 Questions

### Q1 Have you used Tor before? If so, how often?

[ ] Yes [ ] No

Usage amount: 

[ ] everyday  [ ] once a week  [ ] once a month [ ] occasionally or in specific situation [ ] this is my first time using Tor

#### Q1.2 What do you use Tor for?

### Q2 Do you use a VPN? If so, how often?

[ ] Yes [ ] No

Usage amount: 

[ ] everyday  [ ] once a week  [ ] once a month [ ] occasionally or in specific situation 

#### Q2.1 What VPN do you use? Do you use Tor with VPN?

### Q3 Do you separate your activities between browser?

### Q4 How much do you think you need privacy and security for your online activities?

[ ] 5 - strongly agree [ ] 4 - agree [ ] 3 - some [ ] 2 - disagree [ ] 1 - strongly disagree

