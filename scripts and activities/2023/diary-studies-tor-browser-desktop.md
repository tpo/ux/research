# Diary Studies
You've been invited to participate in a study of Tor Browser for Desktop by The Tor Project. We really appreciate your time and availability to support us with your participation. Thank you very much! 

This test will take up to 30 minutes daily, and it should be performed for 5 consecutive days, at the time of your preference. We ask you to report your test daily to the User Researcher: `provide email here`

Open your computer (desktop or laptop), go to `https://www.torproject.org/download/` (if you can't access the official Tor Project website, try this mirror to download: `https://tor.calyxinstitute.org/download/`) and download the latest version of Tor Browser in your preferred language.

**Getting prepared:** In your computer, open the notepad of your preference to take notes. You can also take notes on paper, and then type it back and send it by email. The task won't take long.

**Warning:** when browsing with Tor Browser, avoid using bank websites, social media or emails that can identify you. Those sites can also block you because you're using Tor Browser if you don't have two-factor-authentication enabled.

#### ⚠️ If you have a VPN, make sure it is disabled during this test.

### Diary Studies - Day 1
- Date:
- Local Time:
- Operating System (Example: Windows 10, Debian Linux Stretch, etc):

##### 1. Install Tor Browser on your device and open it.
Check the Tor Browser version. Go to `Tor Browser` > `About Tor Browser`

Take notes of the browser version you're using, close the About window and proceed.
- Tor Browser version:

If you're not in a censored region, before starting Tor Browser connection, we will set it up to emulate a censored environment. 

On the address bar type: `about:config`

It will come to the configuration web page for your Tor Browser, please click on `Accept the Risk` and `Continue`.

On the bar Search preference name type: `torbrowser.debug.censorship_level`

Click on the pencil, choose `number` and change the `0` to `1` (without the quotes).

You can now click on the `broom` (`New Identity button`) in the toolbar to restart Tor Browser and proceed with the task. (`add screenshot`)

##### 2. Take a note of when (the timestamp) you opened Tor Browser.

##### 3. Try to connect to the Tor network.

Take a note of when you clicked on connect in Tor Browser, and any other notes that you find useful. Example: _Clicked on Connect at 09:32:02 AM, a colorful line appeared showing the progress of the connection, then..._

##### 4. What happens?

Please share all the steps you had to perform when trying to connect to the Tor network. Take notes of when it starts to try to connect, when it fails and/or when it succeeds:

a. Does it connect? If so, when did it connect at first?

b. Did it fail? When did it fail? What did you do when it failed?

c. Did you choose the country you're connecting (emulating the connection) from? How did it work? How long does it take to connect?

Now we have two separate sections: a) If the connection failed; and b) If you are connected to Tor. Please jump to the section that better suits your connection status.

#### If the connection failed
Please provide us with a screenshot of the results of your browser. You can take a screenshot on your computer and attach it to the report email you're going to send.

##### Troubleshooting
Go to `Settings` > `Connection`, and try a bridge that would work better for you. If you want to choose from one of Tor Browser’s built-in bridges, Snowflake is known to work better for users in censored regions.

Please take notes of this step and let us know if it worked and how long it took to connect.

If the connection failed again, you can try to configure your connection manually using this bridge this bridge: `add bridge address here`  Bridge-moji: 🍒🌻🐵🌵

Please describe the process of trying to connect manually with as many details as possible, and if this works.

Did anything else happen? Please share with us how the process goes. If you want to share a screenshot, please send it to us by email.

If you weren't able to connect to the Tor network anyway, please, detail your experience with as much detail as possible and we will get back to you as soon as possible.

#### If you connected to Tor

##### 5. Please check your Tor circuit and share the name of your guard node with us.
We want to know if you are connecting to regular relays or any type of bridges. The guard node is the first relay in your Tor circuit, which is composed of three relays (guard, middle and exit). 

When you check your Tor circuit you see this information: `This Browser > Bridge: type of bridge (Guard node) > Country (Middle node) > Country (exit node) > Website`. To check your Tor circuit, click on the circuit icon next to the address bar. Share if us only the name of the guard node, i.e. Bridge: Obsf4 or Bridge: Snowflake.

##### 6. How long did it take to connect?

##### 7. On a scale 1 to 5, with 1 being barely connected and 5 being very fast, how would you rate your connection speed using Tor Browser right now?

##### 8. How do you think Tor Browser can be useful for your online activities today?

##### 9. Add any questions you have about Tor here.


#### Diary Studies - Day 2

⚠️ If you have a VPN, make sure it is disabled during this test.

- Date:
- Local Time:
- Operating System (Example: Windows 10, Debian Linux Stretch, etc):

**If you were not able to connect to Tor the day before**, we will get back to you by email with specific instructions to your case. 

**If you are connected to Tor**, please use this moment to test Tor Browser even more. Spend some time of your day using Tor Browser.

##### 1. How's the Tor Browser speed today? If Tor Browser is a bit slow, you can change your Tor circuit to try another connection experience. Please share how it goes with us.

##### 2. Did you check your circuit? Please check your Tor circuit and share the name of your guard node with us.

##### 3. On a scale 1 to 5, with 1 being barely connected and 5 being very fast, how would you rate your connection speed using Tor Browser right now?

##### 4. Did you change your circuit at any point? Why? How did you feel about changing the circuit?

##### 5. How do you think Tor Browser can be useful for your online activities today?

##### 6. Add any questions/concerns/notes you have about Tor here.

#### Diary Studies - Day 3

⚠️ If you have a VPN, make sure it is disabled during this test.
- Date:
- Local Time:
- Operating System (Example: Windows 10, Debian Linux Stretch, etc):

**If you were not able to connect to Tor the day before**, we will get back to you by email with specific instructions to your case. 

**If you are connected to Tor**, please use this moment to test Tor Browser even more. Spend some time of your day using Tor Browser.

##### 1. How long did it take to connect?

##### 2. How's the Tor Browser speed today? If Tor Browser is a bit slow, you can change your Tor circuit to try another connection experience. Please share how it goes with us.

##### 3. Did you check your circuit? Please check your Tor circuit and share the name of your guard node with us.

##### 4. Are you using the same bridge as the first day?

##### 5. It's your third day using Tor Browser everyday, how's your experience going? 

##### 6. On a scale 1 to 5, with 1 being barely connected and 5 being very fast, how would you rate your connection speed using Tor Browser right now?

##### 7. Add any questions you have about Tor here.

#### Diary Studies - Day 4

⚠️ If you have a VPN, make sure it is disabled during this test.
- Date:
- Local Time:
- Operating System (Example: Windows 10, Debian Linux Stretch, etc):

**If you were not able to connect to Tor the day before**, we will get back to you by email with specific instructions to your case. 

**If you are connected to Tor**, please use this moment to test Tor Browser even more. Spend some time of your day using Tor Browser.

##### 1. How's the Tor Browser speed today? If Tor Browser is a bit slow, you can change your Tor circuit to try another connection experience. Please share how it goes with us.
##### 2. Are you using the same bridge as the first day? Please check your Tor circuit and share the name of your guard node with us.
##### 3. Please tell us broadly what you've been using Tor Browser in your online activities, for example: for research, I can use Tor Browser because…
##### 4. If possible, also share how you perform these activities listed on the question above if you were not using Tor, what browser do you use? Are there any specific reasons for using the other browser?
##### 5. On a scale 1 to 5, with 1 being barely connected and 5 being very fast, how would you rate your connection speed using Tor Browser right now?

#### Diary Studies - Day 5

⚠️ If you have a VPN, make sure it is disabled during this test.
- Date:
- Local Time:
- Operating System (Example: Windows 10, Debian Linux Stretch, etc):

**If you were not able to connect to Tor the day before**, we will get back to you by email with specific instructions to your case. 

**If you are connected to Tor**, please use this moment to test Tor Browser even more. It's your last day testing Tor Browser for this study, spend as much time as possible using Tor Browser.

##### 1. How's the Tor Browser speed today? If Tor Browser is a bit slow, you can change your Tor circuit to try another connection experience. Please share how it goes with us.
##### 2. Are you using the same bridge as the first day? Please check your Tor circuit and share the name of your guard node with us.
##### 3. What was the hardest part about using Tor Browser these days?
##### 4. Please tell us broadly what you've been using Tor Browser in your online activities for the last few days. 
##### 5. Which parts of using Tor Browser were you unsure about/lacked confidence in?
##### 6. How did you feel while using Tor Browser? Why did you feel that way? How could that experience have been better?
##### 7. On a scale 1 to 5, with 1 being barely connected and 5 being very fast, how would you rate your connection speed using Tor Browser right now?
##### 8. After using Tor Browser for 5 consecutive days, please rate from 1 to 10, how likely is it that you would use Tor Browser daily? Why?
##### 9. Please add any notes and/or questions you have about Tor here.

You finished your testing. We appreciate your time and availability. Thank you very much!

If you changed your configuration to emulated a censored environment, remember to change your configuration back:

On the address bar type: `about:config`

It will come to the configuration web page for your Tor Browser, please click on `Accept the Risk` and `Continue`.

On the bar Search preference name type: `torbrowser.debug.censorship_level`

Click on the pencil and change the `1` to `0` (without the quotes).
