# User personas 

User personas are a key component of our human-centered design process. They help foster empathy by encouraging our developers to better understand the motivations, pain points, and goals of our users.

They were first created following a series of field studies in the Global South from 2018 to 2019, where we were fortunate to meet many different kinds of Tor users – including activists, journalists and other human rights defenders.

**As of 2023, some of the information presented in the older personas (e.g. the social/political context and state of censorship in some countries) may be out of date. As such, these personas are due to be replaced in the near future.**
